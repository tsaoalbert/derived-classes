
/*
Lab description:

1. Using the given starter program, create an additional derived class (sub class) of type Employee. 
  The Employee class must be derived from the Person class and include the following data items:
    int  EmpNo;    // Employee number
    double Hours;
    double PayRate;

2. The print method for Employee needs to compute the pay based on Hours and PayRate and then display the Name, and Pay.

3. Update the main program to include at least ten more employee. 

4. Use the function objects to sort the employee in terms of their first name, last name, and monthly payment.



*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>
using namespace std;

////////////// Person class Definition /////////////////
class Person {
protected:  // A derived class can access protected data
    string Name;   
public:
    Person (string n) 
    {
        // strcpy(Name, n);
        (Name =  n);
    }
    virtual double pay() const = 0 ;
    virtual void print() const
    {
        cout << "Person:  " << Name << endl;
    }
};

///////////// Student class Definition //////////////
class Student : public Person  {
private:
    int    units;
public:
    // Student constructor gets name from Person class
    Student (string n,int u) : Person ( n)
    {
        units = u;
    }
    virtual double  pay() const override { return 0; } 
    virtual void print() const override
    {
        cout << "Student: " << Name << "  Units: " << units << endl;
    }
};

//////////// Teacher class Definition /////////////////
class Teacher : public Person  {
  private:
    int    numberOfStudents;
    int    numberOfClasses;
    double salary; 
  public:
        // Teacher constructor gets 'name' from the Person class
    Teacher (string n,int s, int c, double pay=5000) : Person ( n)
    {
      numberOfStudents = s;
      numberOfClasses = c;
      salary = pay;
    }
    double pay() const override { return salary; } 
    virtual void print() const override
    {
      cout << "Teacher: " << Name 
        << "  Students: " << numberOfStudents 
        << "  Classes: " << numberOfClasses << endl;
    }
};

//////////// Employee class Definition /////////////////
class Employee : public Person  {
    private:
    int    empNo;
    double hours;
    double payRate; 
    public:
      // Employee constructor gets 'name' from the Person class 
      Employee (string n,int en, double h, double pr) : Person ( n) 
      { 
        empNo = en;
        hours = h;
        payRate = pr; 
      }
    double pay() const  override /* Compute regHours */
    {
      // TODO: add the missing codes here
      return 6000;
    }
    virtual void print() const override
    { 
      cout << "Employee: " << Name
      << "  Pay: $" << fixed << setprecision(2) << pay() << endl;
    }
};

enum class CompareMode {
  PAY, FIRST, LAST
};

class MyCompare 
{
  CompareMode m_mode ;
public:
  MyCompare ( CompareMode m= CompareMode::PAY) {
    m_mode = m ;
  }
  void setMode ( CompareMode m ) {
    m_mode = m;
  }
  bool operator() ( const Person* x, const Person* y ) const {
    switch (m_mode ) {
      case CompareMode::PAY:
      case CompareMode::FIRST:
      case CompareMode::LAST:
        return x->pay() < y->pay();
      default:
        return x->pay() < y->pay();
    }
  }
};
/////////// main program ///////////////////////
int main(int argc, char* argv[])
{
    // create objects from several different types of classes
  Student s1("Joe Williams", 12);
  Student s2("Mary Smith  ", 9);
  Student s3("Tam  Nguyen ", 10);
  Student s4("Jose Chavez ", 11);
  Teacher t1("Joe  Thomas ", 28, 3);
  Teacher t3("Fred Jones  ", 18, 2);

  // TODO: add more employee
  Employee e1("            ", 1 , 1,1);
  Employee e2("            ", 2 , 2,2);

  // Create an array of pointers to different people
  vector< Person* >v  = { &t1, &s2, &s3, &s4, &s2, &t3, &e1, &e2 };  

  MyCompare cmp (CompareMode::PAY) ; // create a function object as the comparison criteria
  sort (begin(v), end(v), cmp );
  cmp.setMode (CompareMode::FIRST) ; // set to another comparison mode.
  sort (begin(v), end(v), cmp );

  for ( auto& x:v ) {
    x->print ();
  }
  return 0;
}


